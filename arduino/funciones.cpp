#include <Arduino.h>
#include <SPI.h>
#include <RF24.h>

// PARAMETROS
RF24 radio(9, 10);  // Pin CE y CSN
const byte direccion[6] = {0xCC, 0xCE, 0xCC, 0xCE, 0xCC};

const int pinSVVerde = 2;
const int pinSVAmarillo = 3;
const int pinSVRojo = 4;

const int pinSHVerde = 5;
const int pinSHAmarillo = 6;
const int pinSHRojo = 7;

// SETUP
void init_pines()
{
  pinMode(pinSVVerde, OUTPUT);
  pinMode(pinSVAmarillo, OUTPUT);
  pinMode(pinSVRojo, OUTPUT);

  pinMode(pinSHVerde, OUTPUT);
  pinMode(pinSHAmarillo, OUTPUT);
  pinMode(pinSHRojo, OUTPUT);
}

void init_rf()
{
  radio.begin();
  radio.openWritingPipe(direccion);  // Dirección del receptor
  radio.setPALevel(RF24_PA_HIGH);       // Potencia de transmisión alta
}

// LOOP
void SVverde_SHrojo() 
{
  // SV en verde, SH en rojo
  digitalWrite(pinSVVerde, HIGH);
  digitalWrite(pinSVAmarillo, LOW);
  digitalWrite(pinSVRojo, LOW);
  
  digitalWrite(pinSHVerde, LOW);
  digitalWrite(pinSHAmarillo, LOW);
  digitalWrite(pinSHRojo, HIGH);
}

void SVamarillo_SHrojo()
{
  // SV en amarillo, SH en rojo
  digitalWrite(pinSVVerde, LOW);
  digitalWrite(pinSVAmarillo, HIGH);
  digitalWrite(pinSVRojo, LOW);
  
  digitalWrite(pinSHVerde, LOW);
  digitalWrite(pinSHAmarillo, LOW);
  digitalWrite(pinSHRojo, HIGH);
}

void SVrojo_SHverde()
{
  // SV en rojo, SH en verde
  digitalWrite(pinSVVerde, LOW);
  digitalWrite(pinSVAmarillo, LOW);
  digitalWrite(pinSVRojo, HIGH);
  
  digitalWrite(pinSHVerde, HIGH);
  digitalWrite(pinSHAmarillo, LOW);
  digitalWrite(pinSHRojo, LOW);
}

void SHamarillo_SVrojo()
{
  // SH en amarillo, SV en rojo
  digitalWrite(pinSVVerde, LOW);
  digitalWrite(pinSVAmarillo, LOW);
  digitalWrite(pinSVRojo, HIGH);
  
  digitalWrite(pinSHVerde, LOW);
  digitalWrite(pinSHAmarillo, HIGH);
  digitalWrite(pinSHRojo, LOW);
}

void printStatus(String status) 
{
  Serial.println(status);
  radio.write(&status, sizeof(status));
}
