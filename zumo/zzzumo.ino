#include <Zumo32U4.h>
#include <SPI.h>
#include <RF24.h>
#include <Wire.h>
#include <Zumo32U4.h>
#include "funciones.h"

/* Este codigo corresponde a un seguider de linea PID, que sigue una linea negra en un ambiente de color blanco. 
Este codigo funciona para un zumo con motores 75:1, para otro tipo de robot, se deben realizar modificaciones.

Esta demostración requiere que se instale una placa de sensores frontales de Zumo 32U4.
Estos deben estar configurados en el pin 4 a DN4 y el pin 20 a DN2. */

// Definir un valor menor a 400 limitara la velocidad maxima a la que se movera el robot.
const uint16_t maxSpeed = 0;

Zumo32U4Buzzer buzzer;
Zumo32U4LineSensors lineSensors;
Zumo32U4Motors motors;
Zumo32U4ButtonA buttonA;
//Zumo32U4LCD display;

int16_t lastError = 0;
int posicion = 5;

#define NUM_SENSORS 5
unsigned int lineSensorValues[NUM_SENSORS];


// PARAMETROS
RF24 MyRadio(0, 1);  // Pin CE y CSN
const byte direccion[6] = "00001";

void calibrateSensors()
{

  // Codigo de autocalibracion de los sensores seguidores de linea.
  delay(1000);
  for(uint16_t i = 0; i < 120; i++)
  {
    if (i > 30 && i <= 90)
    {
      motors.setSpeeds(-200, 200);
    }
    else
    {
      motors.setSpeeds(200, -200);
    }

    lineSensors.calibrate();
  }
  motors.setSpeeds(0, 0);
}


void setup()
{ 
  Serial.begin(9600);  // Inicializa la comunicación serial
  // init_rf();
  MyRadio.openReadingPipe(0,direccion);
  MyRadio.setPALevel(RF24_PA_HIGH);
  MyRadio.startListening();

  lineSensors.initFiveSensors();

  // Reproduce un sonido de bienvenida
  buzzer.play(">g32>>c32");

  // Wait for button A to be pressed and released.
  buttonA.waitForButton();

  calibrateSensors();


  // Reproduce un sonido de confirmacion, luego espera que termine este para comenzar.
  for (int i = 0; i < 3; i++)
  {
    delay(1000);
    buzzer.playNote(NOTE_G(3), 200, 15);
  }
  delay(1000);
  buzzer.playNote(NOTE_G(4), 500, 15);
  delay(1000);
  while(buzzer.isPlaying());
}

void loop()
{
  int16_t position = lineSensors.readLine(lineSensorValues);
  // el "error" es que tan lejos estamos del centro de la
  // linea, en donde corresponde al valor 2000.
  int16_t error = position - 2000;

  // Obtener la diferencia de velocidad del motor utilizando términos PID proporcionales y derivativos
  // (por lo general, el término integral no es muy útil para seguir líneas). Aquí estamos usando una constante proporcional
  // de 1/4 y una constante derivativa de 6, lo que debería funcionar bastante bien para muchas opciones de motores Zumo.
  // Probablemente querrás usar el método de prueba y error para ajustar estas constantes para tu
  // Zumo en particular y el trazado de la línea.
  int16_t speedDifference = error / 4 + 8 * (error - lastError);

  lastError = error;

  // Velocidades individuales de cada motor.  el signo de speedDifference
  // determina si el motor se movera a la izquierda o derecha.
  int16_t leftSpeed = (int16_t)maxSpeed + speedDifference;
  int16_t rightSpeed = (int16_t)maxSpeed - speedDifference;

  /* Limitamos nuestras velocidades de motor para que estén entre 0 y maxSpeed.
  Un motor siempre girará a maxSpeed y el otro
  girará a maxSpeed - |speedDifference| si es positivo,
  de lo contrario, se quedará inmóvil. Para algunas aplicaciones, 
  es posible que desees permitir que la velocidad del motor sea negativa
  para que pueda girar en reversa. */
  leftSpeed = constrain(leftSpeed, 0, (int16_t)maxSpeed);
  rightSpeed = constrain(rightSpeed, 0, (int16_t)maxSpeed);

  motors.setSpeeds(leftSpeed, rightSpeed);


  Datos valor;
  // Datos valor = lectura_rf();
  if (MyRadio.available())
  {
    char texto[3];
    // Lee el mensaje
    MyRadio.read(&texto, sizeof(texto));
    Serial.println(texto);
    char dig1 = texto[0];
    char dig2 = texto[1];
    valor.s1 = dig1 - '0';
    valor.s2 = dig2 - '0';

    Serial.print("primero: ");
    Serial.println(valor.s1);
    Serial.print("segundo: ");
    Serial.println(valor.s2);
  }


  if (lineSensorValues[0] > 500 && lineSensorValues[NUM_SENSORS - 2] > 500 && posicion == 1) 
  { 
    switch (valor.s1) 
    {
      case 0:
      motors.setSpeeds(leftSpeed, rightSpeed); // Valor máximo es 400.
      posicion = 2;
      break;
      case 1:
      motors.setSpeeds(0, 0); // Se detienen los motores.
      break;
      case 2:
      motors.setSpeeds(0, 0); // Se detienen los motores.
    }
  }

  if (lineSensorValues[5] > 500 && lineSensorValues[NUM_SENSORS - 2] > 500 && posicion == 2) 
  { 
    switch (valor.s2) 
    {
      case 0:
      motors.setSpeeds(leftSpeed, rightSpeed); // Valor máximo es 400.
      posicion = 1;
      break;
      case 1:
      motors.setSpeeds(0, 0); // Se detienen los motores.
      break;
      case 2:
      motors.setSpeeds(0, 0); // Se detienen los motores.
    }
  }

}